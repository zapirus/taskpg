package repository

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"

	"zr/internal/models"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "zapirus909"
	dbname   = "test"
)

func DeleteUsers(delete string) {

	psqlconn := fmt.Sprintf("host=%s port=%d dbname=%s user='%s' password=%s sslmode=disable", host, port, dbname, user, password)
	db, err := sql.Open("postgres", psqlconn)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	e, _ := db.Query("DELEtE from Users where id = $1", delete)
	fmt.Println(e)
	return

}

func CreateUser(us models.Users) {

	psqlconn := fmt.Sprintf("host=%s port=%d dbname=%s user='%s' password=%s sslmode=disable", host, port, dbname, user, password)
	db, err := sql.Open("postgres", psqlconn)
	if err != nil {
		log.Fatal(err)
	}

	s := `INSERT INTO Users (id, first_name, last_name, age, email, phone, address, city, country) VALUES ($1, $2, $3, $4, $5, $6, $7. $8, $9)`
	sd, err := db.Exec(s, us.Id, us.FirstName, us.LastName, us.Age, us.Email, us.Phone, us.Address, us.City, us.Country)
	if err != nil {
		panic(err)

	}

	fmt.Println(sd)

}

func GetUsers() ([]models.Users, error) {

	psqlconn := fmt.Sprintf("host=%s port=%d dbname=%s user='%s' password=%s sslmode=disable", host, port, dbname, user, password)
	db, err := sql.Open("postgres", psqlconn)
	if err != nil {
		log.Fatal(err)
	}

	res, err := db.Query("SELECT * from Users")

	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	var tArr []models.Users
	for res.Next() {
		var us models.Users

		res.Scan(&us.Id, &us.LastName, &us.FirstName, &us.Age, &us.Email, &us.Phone, &us.Address, &us.City, &us.Country)
		if err != nil {
			log.Fatal(err)
		}
		tArr = append(tArr, us)

	}

	return tArr, err

}

func UpdateUsers(users models.Users) {
	psqlconn := fmt.Sprintf("host=%s port=%d dbname=%s user='%s' password=%s sslmode=disable", host, port, dbname, user, password)
	db, err := sql.Open("postgres", psqlconn)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	es, _ := db.Query("UPDATE Users SET last_name= $1, first_name = $2 WHERE id= $2", users.LastName, users.FirstName, users.Id)
	fmt.Println(es)
	return
}
