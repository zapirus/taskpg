package service

import (
	"zr/internal/models"
	"zr/internal/repository"
)

func CreateUser(users models.Users) {
	repository.CreateUser(users)
}

func GetUsers() []models.Users {
	r, _ := repository.GetUsers()
	return r
}

func DeleteUsers(id string) {
	repository.DeleteUsers(id)

}

func UpdateUsers(users models.Users) {
	repository.UpdateUsers(users)

}
