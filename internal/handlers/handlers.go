package handlers

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"

	"zr/internal/models"
	"zr/internal/service"
)

type APIServer struct {
	config *Config
	router *mux.Router
	logger *logrus.Logger
}

func New(config *Config) *APIServer {
	return &APIServer{
		config: config,
		router: mux.NewRouter(),
		logger: logrus.New(),
	}
}

func (s *APIServer) Run() {
	srv := &http.Server{
		Addr:    s.config.HTTPAddr,
		Handler: s.router,
	}

	s.confRouter()
	s.logger.Printf("Завелись на порту %s", s.config.HTTPAddr)
	idConnClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt, syscall.SIGTERM)
		<-sigint
		ctx, cancel := context.WithTimeout(context.Background(), 4*time.Second)
		defer cancel()
		if err := srv.Shutdown(ctx); err != nil {
			s.logger.Fatalln(err)
		}
		close(idConnClosed)
	}()
	if err := srv.ListenAndServe(); err != nil {
		if !errors.Is(err, http.ErrServerClosed) {
			s.logger.Fatalln(err)
		}
	}
	<-idConnClosed
	s.logger.Println("Всего доброго!")
}

func (s *APIServer) confRouter() {
	// роуты юзера
	s.router.HandleFunc("/create", s.CreateUser()).Methods("POST")
	s.router.HandleFunc("/get", s.GetUsers()).Methods("GET")
	s.router.HandleFunc("/update/{id}", s.UpdateUsers()).Methods("POST")
	s.router.HandleFunc("/delete/{id}", s.DeleteUser()).Methods("DELETE")

}

func (s *APIServer) CreateUser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Content-Type", "application/json")
		var newUser models.Users
		if err := json.NewDecoder(r.Body).Decode(&newUser); err != nil {
			s.logger.Printf("Не удалось преаброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
		service.CreateUser(newUser)

	}
}

func (s *APIServer) GetUsers() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Content-Type", "application/json")
		res := service.GetUsers()
		if err := json.NewEncoder(w).Encode(res); err != nil {
			s.logger.Printf("Не удалось преаброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
	}
}

func (s *APIServer) UpdateUsers() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Content-Type", "application/json")
		var newUser models.Users
		if err := json.NewDecoder(r.Body).Decode(&newUser); err != nil {
			s.logger.Printf("Не удалось преаброзовать: %s", err)
			w.WriteHeader(http.StatusConflict)
			return
		}
		service.UpdateUsers(newUser)

	}
}

func (s *APIServer) DeleteUser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var reqId = mux.Vars(r)["id"]

		w.Header().Set("Content-Type", "application/json")
		service.DeleteUsers(reqId)

	}
}
